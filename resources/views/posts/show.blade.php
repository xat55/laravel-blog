@extends('posts.main')

@section('content')
<a class="btn btn-outline-warning mt-2" href="{{ route('posts.index') }}">Назад к списку статей</a>
<div class="card mt-2">
  <div class="card-body">
    <h3>{{ $post->header }}</h3>
    <p>{{ $post->categories()->pluck('name')->implode(' ') }}</p>
    <p><b>{{ $post->text }}</b></p>
  </div>
</div>
@endsection