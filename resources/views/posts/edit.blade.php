@extends('posts.main')

@section('content')
  <div class="mb-3">
    <h3>Отредактируйте вашу статью</h3>
  </div>
  
  <form method="POST" action="{{ route('posts.update', $post) }}">
    @method('PATCH')
    @csrf  
    <div class="form-group">
      <label for="exampleInputEmail1">Текущее имя рубрики</label>
      <input type="name" multiple disabled class="form-control" value="{{ $post->categories()->pluck('name')->implode(', ') }}" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="form-group">
      <label for="exampleFormControlSelect1">Выберите рубрику статьи</label>
      <select class="form-control" name="categories[]" multiple id="exampleFormControlSelect1">
        @forelse($categories as $category)
          <option>{{ $category->name }}</option>
        @empty
          <option>Без категории</option>
        @endforelse
      </select>    
    </div>  
    <div class="form-group">
      <label for="exampleInputEmail1">Заголовок статьи</label>
      <input type="name" name="header" class="form-control" value="{{ $post->header }}" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Текст статьи</label>
      <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="5">{{ $post->text }}</textarea>
    </div>  
    <button type="submit" class="btn btn-primary">Опубликовать</button>
  </form>
@endsection
