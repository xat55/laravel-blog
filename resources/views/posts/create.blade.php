@extends('posts.main')

@section('content')
  <div class="mb-5">
    <h3>Напишите свою статью</h3>
  </div>

  <form method="POST" action="{{ route('posts.store') }}">
    @csrf 
    <div class="form-group">
      <label for="exampleFormControlSelect1">Выберите рубрику статьи</label>
      <select class="form-control" name="categories[]" multiple id="exampleFormControlSelect1">
        @forelse($categories as $category)
          <option>{{ $category->name }}</option>
        @empty
          <option>Без категории</option>
        @endforelse
      </select>    
    </div>
    <div class="form-group">
      <label for="exampleInputHeader1">Заголовок статьи</label>
      <input type="name" name="header" class="form-control" value="{{ old('header') }}" id="exampleInputHeader1" aria-describedby="headerHelp">
    </div>
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Текст статьи</label>
      <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="5">{{ old('text') }}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Опубликовать</button>
  </form>
@endsection
