@extends('posts.main')

@section('content')
  <div class="mb-3">
    <h3>Список ваших статей</h3>
  </div>
  <table class="table table-striped table-bordered table-hover mt-3">
    <thead>
      <tr>
        <th scope="col">№</th>
        <th scope="col">Заголовок</th>
        <th scope="col">Рубрика</th>
        <th scope="col">Статья</th>
        <th scope="col" class="th-view">Действия</th>
      </tr>
    </thead>
    <tbody>    
      @foreach($posts as $post)
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $post->header }}</td>
          <td>{{ $post->categories()->pluck('name')->implode(', ') }}</td>
          <td>{{ $post->text }}</td>
          <td class="d-flex justify-content-center">            
            <a href="{{ route('posts.show', $post) }}" class="btn btn-success ml-1">
              <i class="fa fa-eye"> View</i>
            </a>
            <a href="{{ route('posts.edit', $post) }}" class="btn btn-primary ml-1">
              <i class="fa fa-pencil"> Edit</i>
            </a>
            <form class="ml-1" action="{{ route('posts.destroy', $post) }}" method="post">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">
                <i class="fa fa-trash"> Delete</i>
              </button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection