<!doctype html>
<html lang="en">
<head>    
  <title>Заполните форму</title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
  <!-- BootstrapCDN Font Awesome icons -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href="css/blog.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <div class="row justify-content-md-center mt-2">
      <div class="col-9">
        
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div><br />
        @endif
        
        @if(session()->get('success'))
          <div class="alert alert-success mt-3">
            {{ session()->get('success') }}
          </div>
        @endif
        
        <div class="mb-2">
          <button class="btn btn-outline-info" type="button" name="button">
            Ваше имя: {{ Auth::user()->name }}
          </button>
          <a class="btn btn-outline-success" href="{{ url('main') }}">Перейти на сайт</a>
          <a class="btn btn-outline-primary" href="{{ route('posts.create') }}">Создать статью</a>
          <a class="btn btn-outline-warning" href="{{ route('posts.index') }}">Перечень ваших статей</a>
        </div>
        
        @yield('content')
        
      </div>
    </div>
  </div>
</body>
</html>





