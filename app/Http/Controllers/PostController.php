<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $author = Auth::user()->name;
    $posts = Post::where('author', $author)->get();
    
    return view('posts.index', compact('posts'));
  }
  
  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $categories = Category::all();
    
    return view('posts.create', compact('categories'));
  }
  
  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $request->validate([
      'categories'=>'required|array',
      'header'=>'required',
      'text'=>'required'
    ]);
    
    $user = Auth::user();
    
    $post = new Post([
      'header' => $request->get('header'),
      'text' => $request->get('text'),
      'author' => $user->name
    ]);
    
    $categoryNames = $request->get('categories');
    
    foreach ($categoryNames as $categoryName) {
      $category = Category::where('name', $categoryName)->first();
      $category->posts()->save($post);
    }  
    
    $user->posts()->save($post);
    
    return redirect('posts')->with('success', 'Пост успешно добавлен!');
  }
  
  /**
  * Display the specified resource.
  *
  * @param  \App\Models\Post  $post
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $post = Post::find($id);
    
    return view('posts.show', compact('post'));
  }
  
  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\Post  $post
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $post = Post::find($id);
    $categories = Category::all();
    
    return view('posts.edit', compact('post', 'categories'));
  }
  
  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Post  $post
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $request->validate([
      'categories'=>'required|array',
      'header'=>'required',
      'text'=>'required'
    ]);
    
    $post = Post::find($id);
    $post->categories()->detach();
    $categoryNames = $request->get('categories');
    
    foreach ($categoryNames as $categoryName) {
      $category = Category::where('name', $categoryName)->first();
      $category->posts()->save($post);
    }  
    $post->header =  $request->get('header');
    $post->text =  $request->get('text');
    $post->save();
    
    return redirect('/posts')->with('success', 'Пост успешно отредактирован!');
  }
  
  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\Post  $post
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $post = Post::find($id);
    $post->categories()->detach();
    $post->users()->detach();
    $post->delete();
    
    return redirect('/posts')->with('success', 'Пост удален!');
  }
}