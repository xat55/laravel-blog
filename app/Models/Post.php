<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    
    protected $fillable = [
      'category',
      'header',
      'text',
      'author',
    ];
    
    protected $casts = [
    'is_admin' => 'boolean',
  ];
  
  public function users()
  {
    return $this->belongsToMany('App\Models\User')->withTimestamps();
  }
  
  public function categories()
  {
    return $this->belongsToMany('App\Models\Category')->withTimestamps();
  }
}
