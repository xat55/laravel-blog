<?php

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Если вам необходимо видеть все sql-запросы, выполняемые к БД на всех страницах вашего сайта можно воспользоваться следующей конструкцией:
DB::listen(function($query) {
  // dump($query->sql, $query->bindings, $query->time);
  // dump($query->sql);
});

Route::view('/', 'welcome');

// 

use App\Http\Controllers\ArticleController;
Route::get('main', [ArticleController::class, 'getArticles']);
Route::get('show-posts-author/{author}', [ArticleController::class, 'getArticlesOfAuthor'])->name('showPostsAuthor');



Route::middleware(['auth:sanctum', 'verified'])->group(function () {
  
  Route::resource('posts', App\Http\Controllers\PostController::class);
  
  // Route::view('create', 'posts.create');
  
  // Route::view('/dashboard','posts.main');
  Route::get('/dashboard', function () {
    // return view('dashboard');
    return redirect('main');
  })->name('dashboard');
  
  
  // More routes here
});

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware(['auth'])->name('verification.notice');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/main');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');